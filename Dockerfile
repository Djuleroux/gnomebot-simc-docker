FROM ubuntu:zesty

MAINTAINER Aeg <aegdju@outlook.com>

## SIMC

# Install needed tools & python
RUN apt-get update \
        && apt-get install -y build-essential libssl-dev git locales\
	python3 python3-pip \
	qt5-qmake gcc make g++

# Build Simc
RUN mkdir /app
RUN git clone https://github.com/simulationcraft/simc \
        && cd simc/engine \
        && make OPENSSL=1 optimized \
        && mv /simc/engine/simc /bin/simc \
        && rm -fr /simc

## Fix UTF8
# Ensure that we always use UTF-8 and with french UTF8 (important for python), replace with your own local in utf8
RUN locale-gen fr_FR.UTF-8

COPY Default_locale /etc/default/locale
RUN chmod 0755 /etc/default/locale

ENV LC_ALL=fr_FR.UTF-8
ENV LANG=fr_FR.UTF-8
ENV LANGUAGE=fr_FR.UTF-8

## discord bot

RUN pip3 install git+https://github.com/Rapptz/discord.py@async
RUN pip3 install aiofiles
RUN mkdir /simc
WORKDIR /app
RUN git clone https://Aegdju@gitlab.com/Aegdju/gnomebot.git /app && date

#manual testing 
#COPY help.file help.file
#COPY help2.file help2.file
#COPY simc.py /app/simc.py

#copy your config file (see template in gnomebot src)
COPY user_data.json /app/user_data.json
#copy your apikey.txt (from root repo) for simc
COPY apikey.txt /app/apikey.txt 

CMD ["python3", "/app/simc.py"]
